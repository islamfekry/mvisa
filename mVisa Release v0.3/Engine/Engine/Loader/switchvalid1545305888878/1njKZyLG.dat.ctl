load data infile '1njKZyLG.dat'
badfile '1njKZyLG.dat.bad'
discardfile '1njKZyLG.dat.discard'
append
into table PENDING_TRANSACTION_SWITCH
fields terminated by ';'
(
TRANSACTION_DATE TIMESTAMP WITH TIME ZONE "DY MON dd hh24:mi:ss TZR YYYY",
AMOUNT,
CARD_NO,
COLUMN1,
COLUMN2,
COLUMN3,
COLUMN4,
COLUMN5,
COLUMN6,
CURRENCY,
file_name,
file_type,
REJECTED_REASON,
REFERENCE_NUMBER,
template_name
)