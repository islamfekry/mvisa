load data infile 'wh3hyh65.dat'
badfile 'wh3hyh65.dat.bad'
discardfile 'wh3hyh65.dat.discard'
append
into table REJECTED_TRANSACTION_SWITCH
fields terminated by ';'
(
TRANSACTION_DATE TIMESTAMP WITH TIME ZONE "DY MON dd hh24:mi:ss TZR YYYY",
AMOUNT,
CARD_NO,
COLUMN1,
COLUMN2,
COLUMN3,
COLUMN4,
COLUMN5,
COLUMN6,
CURRENCY,
file_name,
file_type,
REJECTED_REASON,
REFERENCE_NUMBER,
template_name
)