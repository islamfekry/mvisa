load data infile 'c1zGuWXm.dat'
badfile 'c1zGuWXm.dat.bad'
discardfile 'c1zGuWXm.dat.discard'
append
into table PENDING_TRANSACTION_EBCISSUER
fields terminated by ';'
(
ACTION_CODE,
card_acceptor_code,
card_acceptor_terminal,
FEES,
FROM_PAN,
AUTHORIZATION_NO,
REFERENCE_NUMBER,
RESPONSE_CODE,
LEDGER_EFFECT,
oct_rrn,
TRANSACTION_DATE TIMESTAMP WITH TIME ZONE "DY MON dd hh24:mi:ss TZR YYYY",
terminal_location,
TO_PAN,
AMOUNT,
COLUMN1,
file_name,
file_type,
REJECTED_REASON,
template_name
)