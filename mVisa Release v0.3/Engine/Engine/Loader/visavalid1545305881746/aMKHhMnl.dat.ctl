load data infile 'aMKHhMnl.dat'
badfile 'aMKHhMnl.dat.bad'
discardfile 'aMKHhMnl.dat.discard'
append
into table PENDING_TRANSACTION_VISA
fields terminated by ';'
(
CARD_NO,
CURRENCY,
PROCESS_CODE,
REFERENCE_NUMBER,
REPORT_ID,
RESPONSE_CODE,
SETTLEMENT_AMOUNT,
TRACE_NUMBER,
TRANSACTION_DATE TIMESTAMP WITH TIME ZONE "DY MON dd hh24:mi:ss TZR YYYY",
TRANSACTION_TYPE,
AMOUNT,
FEES,
file_name,
file_type,
template_name
)